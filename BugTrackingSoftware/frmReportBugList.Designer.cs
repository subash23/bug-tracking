﻿namespace BugTrackingSoftware
{

    public partial class frmReportBugList : Form
    {
        public frmReportBugList()
        {
            InitializeComponent();
        }

        private void frmReportBugList_Load(object sender, EventArgs e)
        {
            BugAssigned bug = new BugAssigned();
            this.reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource rpt = new ReportDataSource("buglist", bug.BugReportGetAll());
            this.reportViewer1.LocalReport.DataSources.Add(rpt);
            this.reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }
    }
}
