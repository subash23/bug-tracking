﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSoftware.AppCode;
using System.Data.SqlClient;

namespace BugTrackingSoftware
{
    public partial class frmProjectRegistration : Form
    {
        public frmProjectRegistration()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool check = txtProName.Text.Length > 0 &&
                       txtProVersion.Text.Length > 0 &&
                       txtProDesc.Text.Length > 0;

            if (check == true)
            {
                ProjectRegistration objpro = new ProjectRegistration();
                objpro.Name = txtProName.Text;
                objpro.Version = txtProVersion.Text;
                objpro.Description = txtProDesc.Text;

                if (btnSave.Text.Equals("Save"))
                {
                    objpro.Insert();
                    MessageBox.Show("Project Information saved", "Bug Tracker");
                    loadGridContent();
                }

                else
                {
                    objpro.ProjectID = Convert.ToInt32(txtProjectID.Text);
                    objpro.Update();
                    MessageBox.Show("Project Information updated", "Bug Tracker");
                    btnSave.Text = "Save";
                    loadGridContent();
                }

                txtProName.Clear();
                txtProVersion.Clear();
                txtProDesc.Clear();
                txtProjectID.Clear();
                loadGridContent();
            }

            else
            {
                MessageBox.Show("Please fillup all data", "Bug Tracker");
            }


        }
        private void loadGridContent()
        {
            ProjectRegistration objprojectreg = new ProjectRegistration();
            dgvProject.DataSource = null;
            dgvProject.Columns.Clear();
            dgvProject.DataSource = objprojectreg.GetAll();
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            DataGridViewButtonColumn del = new DataGridViewButtonColumn();

            col.Name = "ColEdit";
            col.HeaderText = "Edit";
            col.UseColumnTextForButtonValue = true;
            col.Text = "EDIT";
            dgvProject.Columns.Add(col);

            del.Name = "ColDelete";
            del.HeaderText = "Delete";
            del.UseColumnTextForButtonValue = true;
            del.Text = "DELETE";
            dgvProject.Columns.Add(del);
            dgvProject.Columns["iProjectID"].Visible = false;
        }

        private void frmProjectRegistration_Load(object sender, EventArgs e)
        {
            loadGridContent();
            this.ActiveControl = txtProName;
            txtProjectID.Clear();
            txtProName.Clear();
            txtProVersion.Clear();
            txtProDesc.Clear();
        }

        private void dgvProject_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvProject.Columns["ColEdit"].Index == e.ColumnIndex)
            {
                txtProName.Text = dgvProject.Rows[e.RowIndex].Cells["sName"].Value.ToString();
                txtProVersion.Text = dgvProject.Rows[e.RowIndex].Cells["sVersion"].Value.ToString();
                txtProDesc.Text = dgvProject.Rows[e.RowIndex].Cells["sDescription"].Value.ToString();
                txtProjectID.Text = dgvProject.Rows[e.RowIndex].Cells["iProjectID"].Value.ToString();

                btnSave.Text = "Update";
            }
            if (dgvProject.Columns["ColDelete"].Index == e.ColumnIndex)
            {
                if (MessageBox.Show("Do you want to delete the record???", "Bug Tracking Software", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    txtProjectID.Text = dgvProject.Rows[e.RowIndex].Cells["iProjectID"].Value.ToString();
                    ProjectRegistration project = new ProjectRegistration();
                    project.ProjectID = Convert.ToInt16(txtProjectID.Text);
                    project.DeleteByID();
                    MessageBox.Show("Project information deleted", "Bug Tracking Software");
                    loadGridContent();
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtProName.Clear();
            txtProVersion.Clear();
            txtProDesc.Clear();
            txtProjectID.Clear();
        }

        private void txtProVersion_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtProName_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}



