﻿namespace BugTrackingSoftware
{
    partial class frmBugAssign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBugTitle = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBugID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDeveloperList = new System.Windows.Forms.DataGridView();
            this.txtBugDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAssign = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.cmbPriority = new System.Windows.Forms.ComboBox();
            this.currentDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeveloperList)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 73;
            this.label2.Text = "Bug Priority";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 23);
            this.label3.TabIndex = 72;
            this.label3.Text = "Bug Status";
            // 
            // txtBugTitle
            // 
            this.txtBugTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBugTitle.Enabled = false;
            this.txtBugTitle.Location = new System.Drawing.Point(118, 55);
            this.txtBugTitle.Name = "txtBugTitle";
            this.txtBugTitle.Size = new System.Drawing.Size(131, 20);
            this.txtBugTitle.TabIndex = 71;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 23);
            this.label9.TabIndex = 70;
            this.label9.Text = "Bug Title";
            // 
            // txtBugID
            // 
            this.txtBugID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBugID.Enabled = false;
            this.txtBugID.Location = new System.Drawing.Point(118, 16);
            this.txtBugID.Name = "txtBugID";
            this.txtBugID.Size = new System.Drawing.Size(131, 20);
            this.txtBugID.TabIndex = 77;
            this.txtBugID.TextChanged += new System.EventHandler(this.txtBugID_TextChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 76;
            this.label1.Text = "Bug ID";
            // 
            // dgvDeveloperList
            // 
            this.dgvDeveloperList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeveloperList.Location = new System.Drawing.Point(299, 121);
            this.dgvDeveloperList.Name = "dgvDeveloperList";
            this.dgvDeveloperList.ReadOnly = true;
            this.dgvDeveloperList.Size = new System.Drawing.Size(558, 187);
            this.dgvDeveloperList.TabIndex = 78;
            this.dgvDeveloperList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeveloperList_CellContentClick);
            // 
            // txtBugDesc
            // 
            this.txtBugDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBugDesc.Enabled = false;
            this.txtBugDesc.Location = new System.Drawing.Point(118, 169);
            this.txtBugDesc.Multiline = true;
            this.txtBugDesc.Name = "txtBugDesc";
            this.txtBugDesc.Size = new System.Drawing.Size(131, 59);
            this.txtBugDesc.TabIndex = 80;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 23);
            this.label4.TabIndex = 79;
            this.label4.Text = "Bug Description";
            // 
            // btnAssign
            // 
            this.btnAssign.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssign.Image = global::BugTrackingSoftware.Properties.Resources._1454154384_Apply;
            this.btnAssign.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAssign.Location = new System.Drawing.Point(688, 333);
            this.btnAssign.Name = "btnAssign";
            this.btnAssign.Size = new System.Drawing.Size(169, 30);
            this.btnAssign.TabIndex = 81;
            this.btnAssign.Text = "Assign To Developer";
            this.btnAssign.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAssign.UseVisualStyleBackColor = true;
            this.btnAssign.Click += new System.EventHandler(this.btnAssign_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(296, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 23);
            this.label5.TabIndex = 82;
            this.label5.Text = "Date Assigned";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.cmbStatus);
            this.panel1.Controls.Add(this.cmbPriority);
            this.panel1.Controls.Add(this.txtBugDesc);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtBugID);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtBugTitle);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(21, 66);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(269, 242);
            this.panel1.TabIndex = 84;
            // 
            // cmbStatus
            // 
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "--Select Bug Status--",
            "Open",
            "Assign",
            "In Progress",
            "Completed",
            "Closed"});
            this.cmbStatus.Location = new System.Drawing.Point(118, 127);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(131, 21);
            this.cmbStatus.TabIndex = 82;
            // 
            // cmbPriority
            // 
            this.cmbPriority.Enabled = false;
            this.cmbPriority.FormattingEnabled = true;
            this.cmbPriority.Location = new System.Drawing.Point(118, 93);
            this.cmbPriority.Name = "cmbPriority";
            this.cmbPriority.Size = new System.Drawing.Size(131, 21);
            this.cmbPriority.TabIndex = 81;
            // 
            // currentDate
            // 
            this.currentDate.CalendarTitleBackColor = System.Drawing.Color.Chartreuse;
            this.currentDate.Location = new System.Drawing.Point(299, 91);
            this.currentDate.Name = "currentDate";
            this.currentDate.Size = new System.Drawing.Size(219, 20);
            this.currentDate.TabIndex = 85;
            this.currentDate.Value = new System.DateTime(2016, 1, 22, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Fuchsia;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Indigo;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(869, 52);
            this.label7.TabIndex = 86;
            this.label7.Text = "Bug Assign Form";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(793, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 23);
            this.label6.TabIndex = 87;
            this.label6.Text = "Date End";
            // 
            // endDate
            // 
            this.endDate.Location = new System.Drawing.Point(651, 91);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(206, 20);
            this.endDate.TabIndex = 88;
            this.endDate.Value = new System.DateTime(2016, 1, 22, 0, 0, 0, 0);
            // 
            // frmBugAssign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Violet;
            this.ClientSize = new System.Drawing.Size(869, 375);
            this.Controls.Add(this.endDate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.currentDate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnAssign);
            this.Controls.Add(this.dgvDeveloperList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmBugAssign";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bug Assign From";
            this.Load += new System.EventHandler(this.frmBugAssign_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeveloperList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBugTitle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBugID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDeveloperList;
        private System.Windows.Forms.TextBox txtBugDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAssign;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker currentDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.ComboBox cmbPriority;
        private System.Windows.Forms.ComboBox cmbStatus;
    }
}