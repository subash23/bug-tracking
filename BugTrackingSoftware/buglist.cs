﻿namespace BugTrackingSoftware {
    
     namespace BugTrackingSoftware
{
    public partial class frmBugList : Form
    {
        public frmBugList()
        {
            InitializeComponent();

            if (CommonFunctions.UserType.Equals("Project Leader"))
            {
                btnAssign.Enabled = true;
            }
            else
            {
                btnAssign.Visible = false;
            }

        }

        private void frmBugList_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Today;
            cmbStatus.SelectedIndex = 0;
            loadGridContent();
            txtBugName.Focus();

        }
        public void loadGridContent()
        {
            if (CommonFunctions.UserType.Equals("Project Leader"))
            {
                BugRegistration obj = new BugRegistration();
                obj.UserID = CommonFunctions.UserID;
                dgvBugList.DataSource = null;
                dgvBugList.Columns.Clear();
                dgvBugList.DataSource = obj.GetAll();
            }

            else if (CommonFunctions.UserType.Equals("Tester"))
            {
                BugRegistration obj = new BugRegistration();
                obj.UserID = CommonFunctions.UserID;
                dgvBugList.DataSource = null;
                dgvBugList.Columns.Clear();
                dgvBugList.DataSource = obj.BugReportTesterGetByID();
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = dgvBugList.DataSource;
            bs.Filter = "[Bug_Title] like '*" + txtBugName.Text + "*'";
            dgvBugList.DataSource = bs;
        }

        private void btnAssign_Click(object sender, EventArgs e)
        {
            //check data
            bool check = dgvBugList.SelectedRows.Count > 0;
            if (check)
            {
                CommonFunctions.BugID = Convert.ToInt32(dgvBugList.SelectedRows[0].Cells["iBugID"].Value);
                string status = dgvBugList.SelectedRows[0].Cells["Bug_Status"].Value.ToString();

                if (status.Equals("Open"))
                {
                    CommonFunctions.BugID = Convert.ToInt32(dgvBugList.SelectedRows[0].Cells["iBugID"].Value);
                    CommonFunctions.BugTitle = dgvBugList.SelectedRows[0].Cells["Bug_Title"].Value.ToString();
                    CommonFunctions.BugPriority = dgvBugList.SelectedRows[0].Cells["Bug_Priority"].Value.ToString();
                    CommonFunctions.BugStatus = dgvBugList.SelectedRows[0].Cells["Bug_Status"].Value.ToString();
                    CommonFunctions.BugDescription = dgvBugList.SelectedRows[0].Cells["Bug_Description"].Value.ToString();
                    frmBugAssign f = new frmBugAssign();
                    f.ShowDialog();
                    this.Hide();
                }
                else if (status.Equals("Closed"))
                {
                    MessageBox.Show("The bug is already " + status, "Bug Track");
                }
                else if (status.Equals("In Progress"))
                {
                    MessageBox.Show("The bug is already " + status, "Bug Track");
                }
                else if (status.Equals("Assign"))
                {
                    MessageBox.Show("The bug is already " + status, "Bug Track");
                }
                else if (status.Equals("Completed"))
                {
                    MessageBox.Show("The bug is already " + status, "Bug Track");
                }

            }

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtBugName.Clear();
            cmbStatus.SelectedIndex = 0;
            loadGridContent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Close the bug status???", "Bug Tracking Software", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                txtBugID.Text = dgvBugList.SelectedRows[0].Cells["iBugID"].Value.ToString();
                BugRegistration obj = new BugRegistration();
                obj.BugID = Convert.ToInt32(txtBugID.Text);
                obj.Bug_Status = textBox1.Text;
                obj.BugStatusUpdate();
                loadGridContent();
            }

        }

        private void dgvBugList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = dgvBugList.DataSource;
            bs.Filter = "[Bug_Status] like '*" + cmbStatus.SelectedItem + "*'";
        }

        private void dgvBugList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == this.dgvBugList.Columns["Bug_Priority"].Index)
            {
                if (e.Value != null)
                {
                    string Bug_Priority = e.Value.ToString();
                    if (Bug_Priority == "Severe")
                    {
                        this.dgvBugList.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
                    }
                    if (Bug_Priority == "Open")
                    {
                        this.dgvBugList.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.PaleGreen;
                    }
                    if (Bug_Priority == "High")
                    {
                        this.dgvBugList.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                    }
                    if (Bug_Priority == "Low")
                    {
                        this.dgvBugList.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;

                    }
                }
            }
        }

        private void txtBugID_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
        }
    }
}
