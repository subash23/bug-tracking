﻿namespace BugTrackingSoftware
{
    partial class frmBugInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtBugID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLineOCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBugModule = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ResolvedDate = new System.Windows.Forms.DateTimePicker();
            this.picBug = new System.Windows.Forms.PictureBox();
            this.btnAttach = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvBugFixInfo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.picBug)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBugFixInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 23);
            this.label4.TabIndex = 89;
            this.label4.Text = "Resolved Date";
            // 
            // txtBugID
            // 
            this.txtBugID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBugID.Enabled = false;
            this.txtBugID.Location = new System.Drawing.Point(139, 62);
            this.txtBugID.Name = "txtBugID";
            this.txtBugID.Size = new System.Drawing.Size(154, 22);
            this.txtBugID.TabIndex = 88;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 87;
            this.label1.Text = "Bug ID";
            // 
            // txtLineOCode
            // 
            this.txtLineOCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLineOCode.Location = new System.Drawing.Point(139, 137);
            this.txtLineOCode.Name = "txtLineOCode";
            this.txtLineOCode.Size = new System.Drawing.Size(154, 22);
            this.txtLineOCode.TabIndex = 85;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 84;
            this.label2.Text = "Lines of Code";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(311, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 23);
            this.label3.TabIndex = 83;
            this.label3.Text = "Bug Attachment";
            // 
            // txtBugModule
            // 
            this.txtBugModule.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBugModule.Location = new System.Drawing.Point(139, 102);
            this.txtBugModule.Name = "txtBugModule";
            this.txtBugModule.Size = new System.Drawing.Size(154, 22);
            this.txtBugModule.TabIndex = 82;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 23);
            this.label9.TabIndex = 81;
            this.label9.Text = "Module";
            // 
            // txtUserID
            // 
            this.txtUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserID.Enabled = false;
            this.txtUserID.Location = new System.Drawing.Point(139, 25);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(154, 22);
            this.txtUserID.TabIndex = 92;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 91;
            this.label5.Text = "User  ID";
            // 
            // ResolvedDate
            // 
            this.ResolvedDate.Location = new System.Drawing.Point(140, 209);
            this.ResolvedDate.Name = "ResolvedDate";
            this.ResolvedDate.Size = new System.Drawing.Size(190, 22);
            this.ResolvedDate.TabIndex = 93;
            this.ResolvedDate.Value = new System.DateTime(2016, 1, 10, 0, 0, 0, 0);
            // 
            // picBug
            // 
            this.picBug.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBug.Location = new System.Drawing.Point(436, 24);
            this.picBug.Name = "picBug";
            this.picBug.Size = new System.Drawing.Size(205, 176);
            this.picBug.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBug.TabIndex = 94;
            this.picBug.TabStop = false;
            // 
            // btnAttach
            // 
            this.btnAttach.Location = new System.Drawing.Point(355, 171);
            this.btnAttach.Name = "btnAttach";
            this.btnAttach.Size = new System.Drawing.Size(75, 29);
            this.btnAttach.TabIndex = 95;
            this.btnAttach.Text = "Attach";
            this.btnAttach.UseVisualStyleBackColor = true;
            this.btnAttach.Click += new System.EventHandler(this.btnAttach_Click);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 96;
            this.label6.Text = "Status";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = global::BugTrackingSoftware.Properties.Resources._1454154384_Apply;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(536, 325);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(131, 35);
            this.btnSave.TabIndex = 98;
            this.btnSave.Text = "Resolve Bug";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cmbStatus);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnAttach);
            this.panel1.Controls.Add(this.picBug);
            this.panel1.Controls.Add(this.ResolvedDate);
            this.panel1.Controls.Add(this.txtUserID);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtBugID);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtLineOCode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtBugModule);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(25, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(662, 240);
            this.panel1.TabIndex = 99;
            // 
            // cmbStatus
            // 
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "In Progress",
            "Completed"});
            this.cmbStatus.Location = new System.Drawing.Point(140, 172);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(153, 24);
            this.cmbStatus.TabIndex = 97;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Fuchsia;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Indigo;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(724, 48);
            this.label7.TabIndex = 100;
            this.label7.Text = "Solved Bug Information";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvBugFixInfo
            // 
            this.dgvBugFixInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBugFixInfo.Location = new System.Drawing.Point(25, 366);
            this.dgvBugFixInfo.Name = "dgvBugFixInfo";
            this.dgvBugFixInfo.ReadOnly = true;
            this.dgvBugFixInfo.Size = new System.Drawing.Size(661, 148);
            this.dgvBugFixInfo.TabIndex = 101;
            // 
            // frmBugInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.ClientSize = new System.Drawing.Size(710, 526);
            this.Controls.Add(this.dgvBugFixInfo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSave);
            this.Name = "frmBugInfo";
            this.Text = "Solved Bug Information";
            this.Load += new System.EventHandler(this.frmBugInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBug)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBugFixInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBugID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLineOCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBugModule;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker ResolvedDate;
        private System.Windows.Forms.PictureBox picBug;
        private System.Windows.Forms.Button btnAttach;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvBugFixInfo;
    }
}