﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSoftware.AppCode;

namespace BugTrackingSoftware
{
    public partial class frmDeveloperBugList : Form
    {
        public frmDeveloperBugList()
        {
            InitializeComponent();
            currentDate.Value = DateTime.Today;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmBugInfo info = new frmBugInfo(txtUserID.Text, txtBugID.Text, cmbStatus.SelectedItem.ToString());
            info.ShowDialog();
            this.Hide();
        }
        public void LoadGridContent()
        {
            BugAssigned buga = new BugAssigned();
            buga.UserID = Convert.ToInt32(txtUserID.Text);
            dgvDeveloperList.DataSource = buga.GetBugByUserID().Tables[0];
        }

        private void frmDeveloperBugList_Load(object sender, EventArgs e)
        {
            if (CommonFunctions.UserType.Equals("Developer"))
            {
                txtUserID.Text = CommonFunctions.UserID.ToString();
                txtUserID.Enabled = false;
            }
            LoadGridContent();
        }

        private void dgvDeveloperList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtBugID.Text = dgvDeveloperList.Rows[e.RowIndex].Cells["iBugID"].Value.ToString();
            txtBugTitle.Text = dgvDeveloperList.Rows[e.RowIndex].Cells["Bug_Title"].Value.ToString();
            cmbPriority.Text = dgvDeveloperList.Rows[e.RowIndex].Cells["Bug_Priority"].Value.ToString();
            cmbStatus.Text = dgvDeveloperList.Rows[e.RowIndex].Cells["Bug_Status"].Value.ToString();
            txtBugDesc.Text = dgvDeveloperList.Rows[e.RowIndex].Cells["Bug_Description"].Value.ToString();
            currentDate.MinDate = Convert.ToDateTime(dgvDeveloperList.Rows[e.RowIndex].Cells["AssignDate"].Value);
            endDate.MinDate = Convert.ToDateTime(dgvDeveloperList.Rows[e.RowIndex].Cells["EndDate"].Value);
        }
        private void btnPriority_Click(object sender, EventArgs e)
        {
            BugAssigned bugassinged = new BugAssigned();
            bugassinged.BugID = Convert.ToInt32(txtBugID.Text);
            BugRegistration obj = new BugRegistration();
            obj.BugID = Convert.ToInt32(txtBugID.Text);
            obj.Bug_Status = cmbStatus.SelectedItem.ToString();
            obj.BugStatusUpdate();
            MessageBox.Show("Bug status changed", "Bug Tracker");
            LoadGridContent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
