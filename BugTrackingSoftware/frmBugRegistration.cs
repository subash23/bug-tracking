﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSoftware.AppCode;
using System.IO;
using ColorCode;

namespace BugTrackingSoftware
{
    public partial class frmBugRegistration : Form
    {
        public frmBugRegistration()
        {
            InitializeComponent();

        }

        private void frmBugRegistration_Load(object sender, EventArgs e)
        {


            this.ActiveControl = txtProjectName;
            cmbPriority.SelectedIndex = 0;
            cmbStatus.SelectedIndex = 1;


            ProjectRegistration objpro = new ProjectRegistration();
            lstProName.DataSource = objpro.GetAll();
            lstProName.DisplayMember = "sName";
            lstProName.ValueMember = "iProjectID";
            lstProName.Visible = false;
            txtUserName.Text = CommonFunctions.UserName;
            txtUserID.Text = CommonFunctions.UserID.ToString();

            loadGridContent();
        }

        string imgloc;
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "JPEG Files(*.jpg)|*.jpg|GIF Files(*.gif)|*.gif|All Files(*.*)|*.*";
                ofd.Title = "Select Bug Image";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    imgloc = ofd.FileName.ToString();
                    picBug.ImageLocation = imgloc;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool check = txtBugTitle.Text.Length > 0 &&
                       txtBugDesc.Text.Length > 0 &&
                       txtProjectName.Text.Length > 0 &&
                       cmbStatus.SelectedIndex > 0;

            if (check == true)
            {
                BugRegistration objbug = new BugRegistration();
                objbug.BugID = Convert.ToInt32(txtBugID.Text);
                objbug.UserID = Convert.ToInt32(txtUserID.Text);
                objbug.ProjectID = Convert.ToInt32(txtProjectID.Text);
                objbug.Bug_Title = txtBugTitle.Text;
                objbug.Bug_Priority = cmbPriority.Text;
                objbug.Bug_Status = cmbStatus.Text;
                objbug.Bug_Description = txtBugDesc.Text;
                objbug.Bug_Attachment = picBug.Image;

                if (btnSave.Text.Equals("Save"))
                {

                    objbug.Insert();
                    MessageBox.Show("Bug Information saved", "Bug Tracker");
                }

                else
                {
                    objbug.Update();
                    MessageBox.Show("Bug Information updated", "Bug Tracker");
                    btnSave.Text = "Save";
                }

                clear();
                loadGridContent();
            }

            else
            {
                MessageBox.Show("Please fillup all data", "Bug Tracker");
            }
        }

        public void clear()
        {
            txtBugTitle.Clear();
            txtProjectName.Clear();
            lstProName.Visible = false;
            txtProjectID.Clear();
            cmbStatus.SelectedIndex = 0;
            cmbPriority.SelectedIndex = 0; ;
            txtBugDesc.Clear();
            //picBug.Image.Dispose();
            picBug.Image = null;
            btnSave.Text = "Save";
            loadGridContent();
        }

        private void loadGridContent()
        {
            BugRegistration obj = new BugRegistration();
            obj.UserID = CommonFunctions.UserID;
            dgvBugRegistration.DataSource = null;
            dgvBugRegistration.Columns.Clear();
            dgvBugRegistration.DataSource = obj.BugReportTesterGetByID();
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            DataGridViewButtonColumn del = new DataGridViewButtonColumn();

            col.Name = "ColEdit";
            col.HeaderText = "Edit";
            col.UseColumnTextForButtonValue = true;
            col.Text = "EDIT";
            dgvBugRegistration.Columns.Add(col);

            del.Name = "ColDelete";
            del.HeaderText = "Delete";
            del.UseColumnTextForButtonValue = true;
            del.Text = "DELETE";
            dgvBugRegistration.Columns.Add(del);

            ////Rename columns Header
            //dgvBugRegistration.Columns["iBugID"].HeaderText = "Bug ID";
            //dgvBugRegistration.Columns["iProjectID"].HeaderText = "Project ID";
            ////dgvBugRegistration.Columns["sName"].HeaderText = "Project Name";
            ////dgvBugRegistration.Columns["sName1"].HeaderText = "Tester Name";

            //dgvBugRegistration.Columns["iUserID"].HeaderText = "User ID";
            ////dgvBugRegistration.Columns["sUserName"].HeaderText = "User Name";
            //dgvBugRegistration.Columns["Bug_Title"].HeaderText = "Bug Name";
            //dgvBugRegistration.Columns["Bug_Priority"].HeaderText = "Bug Priority";
            ////dgvBugRegistration.Columns["Bug_Staus"].HeaderText = "Bug Status";
            //dgvBugRegistration.Columns["Bug_Description"].HeaderText = "Bug Description";
            //dgvBugRegistration.Columns["Bug_Attachment"].HeaderText = "Bug Picture";

        }

        private void dgvBugRegistration_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvBugRegistration.Columns["ColEdit"].Index == e.ColumnIndex)
            {
                txtBugID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iBugID"].Value.ToString();
                txtProjectID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iProjectID"].Value.ToString();
                txtProjectName.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Project Name"].Value.ToString();
                lstProName.Visible = false;
                txtUserID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iUserID"].Value.ToString();
                txtBugTitle.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Title"].Value.ToString();
                cmbPriority.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Priority"].Value.ToString();
                cmbStatus.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Status"].Value.ToString();
                txtBugDesc.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Description"].Value.ToString();

                byte[] img = (byte[])dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Attachment"].Value;
                MemoryStream ms = new MemoryStream(img);
                picBug.Image = Image.FromStream(ms);

                btnSave.Text = "Update";
            }

            if (dgvBugRegistration.Columns["ColDelete"].Index == e.ColumnIndex)
            {
                if (MessageBox.Show("Do you want to delete the record???", "Bug Tracking Software", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    txtBugID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iBugID"].Value.ToString();
                    BugRegistration bug = new BugRegistration();
                    bug.BugID = Convert.ToInt16(txtBugID.Text);
                    bug.DeleteByID();
                    MessageBox.Show("Bug information deleted", "Bug Tracking Software");
                    loadGridContent();
                }
            }
        }

        private void lstProName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtProjectID.Text = lstProName.SelectedValue.ToString();
                txtProjectName.Text = lstProName.Text;
                lstProName.Visible = false;
            }
        }


        private void txtProjectName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                lstProName.Focus();
            }

            else if (e.KeyCode == Keys.Back)
            {
                lstProName.Visible = false;
            }
        }

        private void txtProjectName_TextChanged_1(object sender, EventArgs e)
        {
            lstProName.Visible = true;
            BindingSource bs = new BindingSource();
            bs.DataSource = lstProName.DataSource;
            bs.Filter = "sName like '*" + txtProjectName.Text + "*' ";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            namespace BugTrackingSoftware
{
    public partial class frmBugRegistration : Form
    {
        public frmBugRegistration()
        {
            InitializeComponent();

        }

        private void frmBugRegistration_Load(object sender, EventArgs e)
        {


            this.ActiveControl = txtProjectName;
            cmbPriority.SelectedIndex = 0;
            cmbStatus.SelectedIndex = 1;


            ProjectRegistration objpro = new ProjectRegistration();
            lstProName.DataSource = objpro.GetAll();
            lstProName.DisplayMember = "sName";
            lstProName.ValueMember = "iProjectID";
            lstProName.Visible = false;
            txtUserName.Text = CommonFunctions.UserName;
            txtUserID.Text = CommonFunctions.UserID.ToString();

            loadGridContent();
        }

        string imgloc;
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "JPEG Files(*.jpg)|*.jpg|GIF Files(*.gif)|*.gif|All Files(*.*)|*.*";
                ofd.Title = "Select Bug Image";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    imgloc = ofd.FileName.ToString();
                    picBug.ImageLocation = imgloc;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool check = txtBugTitle.Text.Length > 0 &&
                       txtBugDesc.Text.Length > 0 &&
                       txtProjectName.Text.Length > 0 &&
                       cmbStatus.SelectedIndex > 0;

            if (check == true)
            {
                BugRegistration objbug = new BugRegistration();
                objbug.BugID = Convert.ToInt32(txtBugID.Text);
                objbug.UserID = Convert.ToInt32(txtUserID.Text);
                objbug.ProjectID = Convert.ToInt32(txtProjectID.Text);
                objbug.Bug_Title = txtBugTitle.Text;
                objbug.Bug_Priority = cmbPriority.Text;
                objbug.Bug_Status = cmbStatus.Text;
                objbug.Bug_Description = txtBugDesc.Text;
                objbug.Bug_Attachment = picBug.Image;

                if (btnSave.Text.Equals("Save"))
                {

                    objbug.Insert();
                    MessageBox.Show("Bug Information saved", "Bug Tracker");
                }

                else
                {
                    objbug.Update();
                    MessageBox.Show("Bug Information updated", "Bug Tracker");
                    btnSave.Text = "Save";
                }

                clear();
                loadGridContent();
            }

            else
            {
                MessageBox.Show("Please fillup all data", "Bug Tracker");
            }
        }

        public void clear()
        {
            txtBugTitle.Clear();
            txtProjectName.Clear();
            lstProName.Visible = false;
            txtProjectID.Clear();
            cmbStatus.SelectedIndex = 0;
            cmbPriority.SelectedIndex = 0; ;
            txtBugDesc.Clear();
            //picBug.Image.Dispose();
            picBug.Image = null;
            btnSave.Text = "Save";
            loadGridContent();
        }

        private void loadGridContent()
        {
            BugRegistration obj = new BugRegistration();
            obj.UserID = CommonFunctions.UserID;
            dgvBugRegistration.DataSource = null;
            dgvBugRegistration.Columns.Clear();
            dgvBugRegistration.DataSource = obj.BugReportTesterGetByID();
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            DataGridViewButtonColumn del = new DataGridViewButtonColumn();

            col.Name = "ColEdit";
            col.HeaderText = "Edit";
            col.UseColumnTextForButtonValue = true;
            col.Text = "EDIT";
            dgvBugRegistration.Columns.Add(col);

            del.Name = "ColDelete";
            del.HeaderText = "Delete";
            del.UseColumnTextForButtonValue = true;
            del.Text = "DELETE";
            dgvBugRegistration.Columns.Add(del);

            ////Rename columns Header
            //dgvBugRegistration.Columns["iBugID"].HeaderText = "Bug ID";
            //dgvBugRegistration.Columns["iProjectID"].HeaderText = "Project ID";
            ////dgvBugRegistration.Columns["sName"].HeaderText = "Project Name";
            ////dgvBugRegistration.Columns["sName1"].HeaderText = "Tester Name";

            //dgvBugRegistration.Columns["iUserID"].HeaderText = "User ID";
            ////dgvBugRegistration.Columns["sUserName"].HeaderText = "User Name";
            //dgvBugRegistration.Columns["Bug_Title"].HeaderText = "Bug Name";
            //dgvBugRegistration.Columns["Bug_Priority"].HeaderText = "Bug Priority";
            ////dgvBugRegistration.Columns["Bug_Staus"].HeaderText = "Bug Status";
            //dgvBugRegistration.Columns["Bug_Description"].HeaderText = "Bug Description";
            //dgvBugRegistration.Columns["Bug_Attachment"].HeaderText = "Bug Picture";

        }

        private void dgvBugRegistration_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvBugRegistration.Columns["ColEdit"].Index == e.ColumnIndex)
            {
                txtBugID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iBugID"].Value.ToString();
                txtProjectID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iProjectID"].Value.ToString();
                txtProjectName.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Project Name"].Value.ToString();
                lstProName.Visible = false;
                txtUserID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iUserID"].Value.ToString();
                txtBugTitle.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Title"].Value.ToString();
                cmbPriority.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Priority"].Value.ToString();
                cmbStatus.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Status"].Value.ToString();
                txtBugDesc.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Description"].Value.ToString();

                byte[] img = (byte[])dgvBugRegistration.Rows[e.RowIndex].Cells["Bug_Attachment"].Value;
                MemoryStream ms = new MemoryStream(img);
                picBug.Image = Image.FromStream(ms);

                btnSave.Text = "Update";
            }

            if (dgvBugRegistration.Columns["ColDelete"].Index == e.ColumnIndex)
            {
                if (MessageBox.Show("Do you want to delete the record???", "Bug Tracking Software", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    txtBugID.Text = dgvBugRegistration.Rows[e.RowIndex].Cells["iBugID"].Value.ToString();
                    BugRegistration bug = new BugRegistration();
                    bug.BugID = Convert.ToInt16(txtBugID.Text);
                    bug.DeleteByID();
                    MessageBox.Show("Bug information deleted", "Bug Tracking Software");
                    loadGridContent();
                }
            }
        }

        private void lstProName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtProjectID.Text = lstProName.SelectedValue.ToString();
                txtProjectName.Text = lstProName.Text;
                lstProName.Visible = false;
            }
        }


        private void txtProjectName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                lstProName.Focus();
            }

            else if (e.KeyCode == Keys.Back)
            {
                lstProName.Visible = false;
            }
        }

        private void txtProjectName_TextChanged_1(object sender, EventArgs e)
        {
            lstProName.Visible = true;
            BindingSource bs = new BindingSource();
            bs.DataSource = lstProName.DataSource;
            bs.Filter = "sName like '*" + txtProjectName.Text + "*' ";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}

        }
    }
}
