﻿namespace BugTrackingSoftware
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbUserType.SelectedIndex = 0;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            UserRegistration objuser = new UserRegistration();
            objuser.UserName = txtUserName.Text;
            objuser.Password = txtPassword.Text;
            objuser.UserType = cmbUserType.SelectedItem.ToString();
            objuser.LoginCheck();
            if (objuser.UserID == 0)
            {
                MessageBox.Show("Invalid username and password", "Bug Tracking Software");
            }
            else
            {
                MessageBox.Show("Welcome  " + objuser.Name, "Bug Tracking Software");
                CommonFunctions.UserName = objuser.UserName;
                CommonFunctions.UserType = objuser.UserType;
                CommonFunctions.UserID = objuser.UserID;
                MainForm mainfrm = new MainForm();
                mainfrm.Show();
                this.Hide();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

