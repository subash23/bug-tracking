﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSoftware.AppCode;

namespace BugTrackingSoftware
{
    public partial class frmBugAssign : Form
    {

        public frmBugAssign()
        {
            InitializeComponent();

        }

        private void frmBugAssign_Load(object sender, EventArgs e)
        {
            txtBugID.Text = CommonFunctions.BugID.ToString();
            txtBugTitle.Text = CommonFunctions.BugTitle.ToString();
            txtBugDesc.Text = CommonFunctions.BugDescription.ToString();
            cmbPriority.Text = CommonFunctions.BugPriority.ToString();
            cmbStatus.SelectedIndex = 2;
            LoadGridContent();
            BugRegistration bug = new BugRegistration();
            bug.BugID = Convert.ToInt16(txtBugID.Text);
            bug.GetByID();

        }

        private void btnAssign_Click(object sender, EventArgs e)
        {
            //check
            bool check = Convert.ToInt32(txtBugID.Text) > 0 &&
                       dgvDeveloperList.SelectedRows.Count > 0;
            //asign
            if (check == true)
            {
                BugAssigned objbugass = new BugAssigned();
                objbugass.BugID = Convert.ToInt32(txtBugID.Text);
                objbugass.UserID = Convert.ToInt32(dgvDeveloperList.SelectedRows[0].Cells["iUserID"].Value);
                objbugass.AssignedDate = currentDate.Value;
                objbugass.EndDate = endDate.Value;
                //save
                objbugass.Insert();
                BugRegistration obj = new BugRegistration();
                obj.BugID = Convert.ToInt32(txtBugID.Text);
                obj.Bug_Status = cmbStatus.SelectedItem.ToString();
                obj.BugStatusUpdate();
                MessageBox.Show("Bug Sucessfully assigned to developer", "Bug Tracker");
                dgvDeveloperList.ClearSelection();

            }
            else
            {
                MessageBox.Show("Error while assigning the bug to the selected developer!", "Bug Tracker");

            }

        }
        public void LoadGridContent()
        {
            UserRegistration objuserreg = new UserRegistration();
            dgvDeveloperList.DataSource = null;
            dgvDeveloperList.Columns.Clear();
            dgvDeveloperList.DataSource = objuserreg.GetAllDevloper();
        }

        private void txtBugID_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvDeveloperList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}