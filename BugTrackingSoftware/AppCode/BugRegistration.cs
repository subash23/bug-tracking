﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTrackingSoftware.AppCode;
using System.Data; //added for the database
using System.Data.SqlClient; //added for sql client connection 
using System.IO; // added for input output
using System.Drawing; //addded for image drawing

namespace BugTrackingSoftware.AppCode
{
    class BugRegistration : Connection
    {
        //creating the properties used for Bug registration class

        public int BugID { get; set; }
        public int ProjectID { get; set; }
        public int UserID { get; set; }
        public string Bug_Title { get; set; }
        public string Bug_Description { get; set; }
        public string Bug_Priority { get; set; }
        public string Bug_Status { get; set; }
        public Image Bug_Attachment { get; set; }

        //initializing the values used for the bug registration class
        public BugRegistration()
        {
            BugID = 0;
            ProjectID = 0;
            UserID = 0;
            Bug_Title = string.Empty;
            Bug_Description = string.Empty;
            Bug_Priority = string.Empty;
            Bug_Status = string.Empty;
            Bug_Attachment = null;

        }

        //methods used for Bug registration class
        public DataTable GetAll()
        {
            //opening the connection for SQL server
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugReportGetAllData";
            //prepare sql command  with respective command type
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);
            //closing the connection
            conn.Close();
            return dt;
        }
        public DataTable GetAllDevloper()
        {
            //opening the connection for SQL server
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.sp_tbDeveloperDatGetByID";
            //prepare sql command  with respective command type
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);
            //closing the connection
            conn.Close();
            //returning the values
            return dt;
        }
        public void Insert()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugReportInsert";
            //set the parameters for the stored procedure
            cmd.Parameters.AddWithValue("@iProjectID", ProjectID);
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Parameters.AddWithValue("@Bug_Title", Bug_Title);
            cmd.Parameters.AddWithValue("@Bug_Description", Bug_Description);
            cmd.Parameters.AddWithValue("@Bug_Priority", Bug_Priority);
            cmd.Parameters.AddWithValue("@Bug_Status", Bug_Status);

            BugRegistration objpic = new BugRegistration();
            cmd.Parameters.AddWithValue("@Bug_Attachment", objpic.ImageToByteArray(Bug_Attachment));

            cmd.Connection = conn;
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();
        }

        public DataTable BugReportTesterGetByID()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "tb_spBugReportTesterGetByID";
            //set the parameters for the stored procedure
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Connection = conn;
            DataTable ds = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);
            //closing the connection
            conn.Close();
            //return the values 
            return ds;
        }

        public void AssignedBugInsert()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugReportInsert";
            //set the parameters for the stored procedure
            cmd.Parameters.AddWithValue("@iBugID", ProjectID);
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Parameters.AddWithValue("@Bug_Priority", Bug_Priority);
            cmd.Parameters.AddWithValue("@Bug_Status", Bug_Status);
            BugRegistration objpic = new BugRegistration();
            cmd.Parameters.AddWithValue("@Bug_Attachment", objpic.ImageToByteArray(Bug_Attachment));
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();
        }
        public void Update()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugReportUpdate";
            //set the parameters for the stored procedure
            cmd.Parameters.AddWithValue("@iBugID", BugID);
            cmd.Parameters.AddWithValue("@iProjectID", ProjectID);
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Parameters.AddWithValue("@Bug_Title", Bug_Title);
            cmd.Parameters.AddWithValue("@Bug_Description", Bug_Description);
            cmd.Parameters.AddWithValue("@Bug_Priority", Bug_Priority);
            cmd.Parameters.AddWithValue("@Bug_Status", Bug_Status);
            BugRegistration objpic = new BugRegistration();
            cmd.Parameters.AddWithValue("@Bug_Attachment", objpic.ImageToByteArray(Bug_Attachment));
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();

        }
        public void DeleteByID()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugReportDeleteByID";
            //set the parameters for the stored procedure
            cmd.Parameters.AddWithValue("@iBugID", BugID);
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();

        }

        public void BugStatusUpdate()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugStatusUpdate";
            //set the parameters for the stored procedure
            cmd.Parameters.AddWithValue("@iBugID", BugID);
            cmd.Parameters.AddWithValue("@Bug_Status", Bug_Status);
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();

        }

        public void GetByID()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugAssignedInfoGetAll";
            //set the parameters for the stored procedure
            cmd.Parameters.AddWithValue("@iBugID", BugID);
            cmd.Connection = conn;
            SqlDataReader sdr;
     
            if (sdr.Read())
            {
                BugID = Convert.ToInt16(sdr["@iBugID"]);
                ProjectID = Convert.ToInt16(sdr["@iProjectID"]);
                UserID = Convert.ToInt16(sdr["@iUserID"]);
                Bug_Title = sdr["Bug_Title"].ToString();
                Bug_Priority = sdr["Bug_priority"].ToString();
                Bug_Status = sdr["Bug_Status"].ToString();
                Bug_Description = sdr["Bug_Description"].ToString();

                BugRegistration obj = new BugRegistration();
                Bug_Attachment = obj.ByteArrayToImage((byte[])sdr["Bug_Attachment"]);
            }
            //execute stored procedure on server with necessary parameters
            conn.Close();
            

        }
        //method for converting image to byte array
        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        //method for converting byte array to image
        public Image ByteArrayToImage(Byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}
