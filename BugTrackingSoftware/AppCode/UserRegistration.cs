﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data; //added for the database
using System.Data.SqlClient; //added for sql client connection 
namespace BugTrackingSoftware.AppCode
{
    class UserRegistration : Connection
    {
        //creating the properties used for User registration class
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }

        //initialization of the values for class
        public UserRegistration()
        {
            UserID = 0;
            Name = string.Empty;
            Address = string.Empty;
            ContactNumber = string.Empty;
            Gender = string.Empty;
            Email = string.Empty;
            UserName = string.Empty;
            Password = string.Empty;
            UserType = string.Empty;
        }

        //methods used for Userregistration class
        public DataTable GetAll()
        {
            //opening the connection for SQL server
            conn.Open();

            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbUserRegistrationGetAll";
            cmd.Connection = conn;

            //prepare sql command  with respective command type
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);

            //closing the connection
            conn.Close();
            //returning the values
            return dt;
        }

        public DataTable GetAllDevloper()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.sp_tbDeveloperDatGetByID";
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);
            //closing the connection
            conn.Close();
            //return values in data table
            return dt;
        }

        public void Insert()
        {
            //opening the connection
            conn.Open();

            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbUserRegistrationInsert";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@sName", Name);
            cmd.Parameters.AddWithValue("@sAddress", Address);
            cmd.Parameters.AddWithValue("@sContactNumber", ContactNumber);
            cmd.Parameters.AddWithValue("@sGender", Gender);
            cmd.Parameters.AddWithValue("@sEmail", Email);
            cmd.Parameters.AddWithValue("@sUserName", UserName);
            cmd.Parameters.AddWithValue("@sPassword", Password);
            cmd.Parameters.AddWithValue("@sUserType", UserType);
            cmd.Connection = conn;

            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();

            //closing the connection
            conn.Close();
        }

        public void Update()
        {
            //opening the connection
            conn.Open();

            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbUserRegistrationUpdate";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Parameters.AddWithValue("@sName", Name);
            cmd.Parameters.AddWithValue("@sAddress", Address);
            cmd.Parameters.AddWithValue("@sContactNumber", ContactNumber);
            cmd.Parameters.AddWithValue("@sGender", Gender);
            cmd.Parameters.AddWithValue("@sEmail", Email);
            cmd.Parameters.AddWithValue("@sUserName", UserName);
            cmd.Parameters.AddWithValue("@sPassword", Password);
            cmd.Parameters.AddWithValue("@sUserType", UserType);
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();

        }
        public void DeleteByID()
        {
            //opening the connection
            conn.Open();

            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbUserRegistrationDeleteByID";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Connection = conn;

            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();

            //closing the connection
            conn.Close();

        }
        public void GetByID()
        {
            //opening the connection
            conn.Open();

            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbUserRegistrationGetByID";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            SqlDataReader sdr;
            sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                UserID = Convert.ToInt16(sdr["@iUserID"]);
                Name = sdr["sName"].ToString();
                Address = sdr["sAddress"].ToString();
                ContactNumber = sdr["sContactNumber"].ToString();
                Gender = sdr["sGender"].ToString();
                Email = sdr["sEmail"].ToString();
                UserName = sdr["sUserName"].ToString();
                Password = sdr["sPassword"].ToString();
                UserType = sdr["sUserType"].ToString();
            }
                //closing the connection
                conn.Close();
            
        }

        public void LoginCheck()
        {
            // opeing the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbUserRegistrationLoginCheck";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@sUserName", UserName);
            cmd.Parameters.AddWithValue("@sPassword", Password);
            cmd.Parameters.AddWithValue("@sUserType", UserType);

            //execute stored procedure on server with necessary parameters
            cmd.Connection = conn;
            SqlDataReader sdr;
            sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                UserID = Convert.ToInt16(sdr["iUserID"]);
                Name = sdr["sName"].ToString();
                UserName = sdr["sUserName"].ToString();
                Password = sdr["sPassword"].ToString();
                UserType = sdr["sUserType"].ToString();
            }
                //closing the connection
                conn.Close();
            
        }
    }
}
