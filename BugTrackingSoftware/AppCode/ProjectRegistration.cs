﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data; //added for the database
using System.Data.SqlClient;//added for sql client connection 

namespace BugTrackingSoftware.AppCode
{
    class ProjectRegistration: Connection
    {
        //creating the properties used for project registration class

        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public string Description { get; set; }

        //initialization of the values

        public ProjectRegistration()
        {
            ProjectID = 0;
            Name = string.Empty;
            Version = string.Empty;
            Description = string.Empty;
            
        }

        //methods used for projectregistration class
        public DataTable GetAll()
        {
            //opening the connection for SQL server
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbProjectInfoGetAll";
            //prepare sql command  with respective command type
            cmd.Connection = conn;

            //fill the table by executing sqlcommand
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);
            //closing the connection
            conn.Close();
            //returning the values
            return dt;
        }
        public void Insert()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbProjectInfoInsert";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@sName", Name);
            cmd.Parameters.AddWithValue("@sVersion", Version);
            cmd.Parameters.AddWithValue("@sDescription", Description);
            cmd.Connection = conn;

            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();

            //closing the connection
            conn.Close();
        }

        public void Update()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbProjectInfoUpdate";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iProjectID", ProjectID);
            cmd.Parameters.AddWithValue("@sName", Name);
            cmd.Parameters.AddWithValue("@sVersion", Version);
            cmd.Parameters.AddWithValue("@sDescription", Description);
            cmd.Connection = conn;

            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();

            //closing the connection
            conn.Close();

        }
        public void DeleteByID()
        {
            //opening the connection
            conn.Open();

            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbProjectInfoDeleteByID";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iProjectID", ProjectID);
            cmd.Connection = conn;

            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();

            //closing the connection
            conn.Close();

        }

        public void GetByID()
        {
            //opening the connection
            conn.Open();

            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbProjectInfoGetByID";

            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iProjectID", ProjectID);
            cmd.Connection = conn;
            SqlDataReader sdr;
            sdr = cmd.ExecuteReader();
            //execute stored procedure on server with necessary parameters
            if (sdr.Read())
            {
                ProjectID = Convert.ToInt16(sdr["@iProjectID"]);
                Name = sdr["sName"].ToString();
                Version = sdr["sVersion"].ToString();
                Description = sdr["sDescription"].ToString();
            }
                //closing the connection
                conn.Close();
            
        }
    }
}
