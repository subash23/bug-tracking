﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;//added for the database
using System.Data.SqlClient; //added for sql client connection 

namespace BugTrackingSoftware.AppCode
{
    class BugAssigned:Connection
    {
        //creating the properties used for Bug assign class

        public int BugID { get; set; }
        public int UserID { get; set; }
        public DateTime AssignedDate  { get; set; }
        public DateTime EndDate { get; set; }
        
        //initializing the values
        public BugAssigned()
        {
            BugID = 0;
            UserID = 0;
            AssignedDate = DateTime.Today;
            EndDate = DateTime.Today;
        }

        //methods used for Bug assign class
        public void Insert()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugAsignedInsert";
            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iBugID", BugID);
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Parameters.AddWithValue("@AssignDate", AssignedDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();
        }

        public DataSet GetBugByUserID()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "tb_spBugAssignedDeveloperGetByID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            //closing the connection
            conn.Close();
            //return value for table data
            return ds;
        }

        public DataTable GetAll()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugAssignedInfoGetAll";
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);
            //closing the connection
            conn.Close();
            //return value for table data
            return dt;
        }

        public DataTable BugProjectLeaderGetAll()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "tb_spBugProjectLeaderGetAll";
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);
            //closing the connection
            conn.Close();
            //return value for table data
            return dt;
        }

        public DataTable BugReportGetAll()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "tb_spBugProjectLeaderGetAll";
            cmd.Connection = conn;
            DataTable dt = new DataTable();
            SqlDataAdapter sdt = new SqlDataAdapter(cmd);
            sdt.Fill(dt);
            //closing the connection
            conn.Close();
            //return value for table data
            return dt;
        }
    }
}
