﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;  //added for the database
using System.Data.SqlClient;  //added for sql client connection 
using System.IO; //added for imput output 
using System.Drawing; //added for image

namespace BugTrackingSoftware.AppCode
{
    class BugInfo:Connection
    {
        //creating the properties used for Bug info class

        public int BugID { get; set; }
        public int UserID { get; set; }
        public string Module { get; set; }
        public string LineOfCode { get; set; }
        public Image BugAttachment { get; set; }
        public DateTime ResolvedDate { get; set; }

        //initialization of the values for class
        public BugInfo()
        {
            BugID = 0;
            UserID = 0;
            Module = string.Empty;
            LineOfCode = string.Empty;
            BugAttachment = null;
            ResolvedDate = DateTime.Today;
        }

        //methods used for the class
        public void Insert()
        {
            //opening the connection
            conn.Open();
            //prepare sql command with respective command type to execute
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_tbBugInfoInsert";
            //set the parameters for the stored procedure 
            cmd.Parameters.AddWithValue("@iBugID", BugID);
            cmd.Parameters.AddWithValue("@iUserID", UserID);
            cmd.Parameters.AddWithValue("@sModule", Module);
            cmd.Parameters.AddWithValue("@sLineOfCode", LineOfCode);
            cmd.Parameters.AddWithValue("@sResolvedDate", ResolvedDate);
            BugInfo objpic = new BugInfo();
            cmd.Parameters.AddWithValue("@BugAttachment", objpic.ImageToByteArray(BugAttachment));
            cmd.Connection = conn;
            //execute stored procedure on server with necessary parameters
            cmd.ExecuteNonQuery();
            //closing the connection
            conn.Close();
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }
        public Image ByteArrayToImage(Byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}
