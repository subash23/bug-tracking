﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackingSoftware.AppCode
{
    class CommonFunctions
    {
        public static string UserName = string.Empty;
        public static string Name = string.Empty;

        public static string UserType = string.Empty;
        public static int UserID = 0;
        public static int BugID = 0;

        public static string BugTitle = string.Empty;
        public static string BugPriority = string.Empty;
        public static string BugStatus = string.Empty;
        public static string BugDescription = string.Empty;

        public static int ReportCustomerID = 0;
    }
}
