﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSoftware.AppCode;
namespace BugTrackingSoftware
{
    public partial class frmBugInfo : Form
    {
        public frmBugInfo(string userid, string bugid, string status)
        {
            InitializeComponent();
            txtUserID.Text = userid;
            txtBugID.Text = bugid;
            cmbStatus.SelectedItem = status;
        }

        public void LoadGridContent()
        {
            BugAssigned buga = new BugAssigned();
            buga.UserID = Convert.ToInt32(txtUserID.Text);
            dgvBugFixInfo.DataSource = buga.GetBugByUserID().Tables[0];
        }
        private void frmBugInfo_Load(object sender, EventArgs e)
        {
            LoadGridContent();
            ResolvedDate.Value = DateTime.Today;
            cmbStatus.SelectedIndex = 1;
        }
        private void btnAttach_Click(object sender, EventArgs e)
        {
            string imgloc;
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "JPEG Files(*.jpg)|*.jpg|GIF Files(*.gif)|*.gif|All Files(*.*)|*.*";
                ofd.Title = "Select Bug Image";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    imgloc = ofd.FileName.ToString();
                    picBug.ImageLocation = imgloc;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool check = txtUserID.Text.Length > 0 &&
                     txtLineOCode.Text.Length > 0 &&
                     txtBugID.Text.Length > 0 &&
                     txtBugModule.Text.Length > 0;

            if (check == true)
            {
                BugInfo objbug = new BugInfo();
                objbug.BugID = Convert.ToInt32(txtBugID.Text);
                objbug.UserID = Convert.ToInt32(txtUserID.Text);
                objbug.Module = txtBugModule.Text;
                objbug.LineOfCode = txtLineOCode.Text;
                objbug.BugAttachment = picBug.Image;
                objbug.ResolvedDate = ResolvedDate.Value;
                objbug.Insert();

                BugRegistration obj = new BugRegistration();
                obj.BugID = Convert.ToInt32(txtBugID.Text);
                obj.Bug_Status = cmbStatus.SelectedItem.ToString();
                obj.BugStatusUpdate();
                MessageBox.Show("Bug successfully completed and saved", "Bug Tracker");
            }
            else
            {
                MessageBox.Show("Please fill all the information");
            }
            LoadGridContent();
        }
    }

}

