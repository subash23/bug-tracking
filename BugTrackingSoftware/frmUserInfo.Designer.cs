﻿namespace BugTrackingSoftware
{
    public partial class frmUserInfo : Form
    {
        public frmUserInfo()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool check = txtName.Text.Length > 0 &&
                        txtAddress.Text.Length > 0 &&
                        cmbGender.SelectedIndex > 0 &&
                        txtContact.Text.Length > 0 &&
                        txtEmail.Text.Length > 0 &&
                        txtUsername.Text.Length > 0 &&
                        txtPassword.Text.Length > 0;

            if (check == true)
            {
                UserRegistration objuser = new UserRegistration();
                objuser.UserID = Convert.ToInt32(txtUserID.Text);
                objuser.Name = txtName.Text;
                objuser.Address = txtAddress.Text;
                objuser.Gender = cmbGender.Text;
                objuser.Email = txtEmail.Text;
                objuser.ContactNumber = txtContact.Text;
                objuser.UserName = txtUsername.Text;
                objuser.Password = txtPassword.Text;
                objuser.UserType = cmbUsertype.Text;

                if (btnSave.Text.Equals("Save"))
                {
                    objuser.Insert();
                    MessageBox.Show("User Information saved", "Bug Tracker");
                    loadGridContent();
                }
                else
                {
                    objuser.Update();
                    MessageBox.Show("User Information updated", "Bug Tracker");
                    btnSave.Text = "Save";
                }

                txtName.Clear();
                txtAddress.Clear();
                txtContact.Clear();
                txtEmail.Clear();
                txtUsername.Clear();
                txtPassword.Clear();
                cmbGender.SelectedIndex = 0;
                cmbUsertype.SelectedIndex = 0;
                loadGridContent();
            }

            else
            {
                MessageBox.Show("Please fillup all data", "Bug Tracker");
            }

        }

        private void frmUserInfo_Load(object sender, EventArgs e)
        {
            cmbGender.SelectedIndex = 0;
            cmbUsertype.SelectedIndex = 0;
            loadGridContent();
        }


        private void loadGridContent()
        {
            UserRegistration objuser = new UserRegistration();
            // UserRegistration objuserreg = new UserRegistration();
            dgvUserRegistration.DataSource = null;
            dgvUserRegistration.Columns.Clear();
            dgvUserRegistration.DataSource = objuser.GetAll();
            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            DataGridViewButtonColumn del = new DataGridViewButtonColumn();

            col.Name = "ColEdit";
            col.HeaderText = "Edit";
            col.UseColumnTextForButtonValue = true;
            col.Text = "EDIT";
            dgvUserRegistration.Columns.Add(col);

            del.Name = "ColDelete";
            del.HeaderText = "Delete";
            del.UseColumnTextForButtonValue = true;
            del.Text = "DELETE";
            dgvUserRegistration.Columns.Add(del);
            dgvUserRegistration.Columns["iUserID"].Visible = false;
        }


        private void dgvUserRegistration_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvUserRegistration.Columns["ColEdit"].Index == e.ColumnIndex)
            {
                txtName.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sName"].Value.ToString();
                txtAddress.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sAddress"].Value.ToString();
                cmbGender.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sGender"].Value.ToString();
                txtEmail.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sEmail"].Value.ToString();
                txtContact.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sContactNumber"].Value.ToString();
                txtUsername.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sUserName"].Value.ToString();
                txtPassword.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sPassword"].Value.ToString();
                cmbUsertype.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["sUserType"].Value.ToString();
                txtUserID.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["iUserID"].Value.ToString();

                btnSave.Text = "Update";
            }
            if (dgvUserRegistration.Columns["ColDelete"].Index == e.ColumnIndex)
            {
                if (MessageBox.Show("Do you want to delete the record???", "Bug Tracking Software", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    txtUserID.Text = dgvUserRegistration.Rows[e.RowIndex].Cells["iUserID"].Value.ToString();
                    UserRegistration user = new UserRegistration();
                    user.UserID = Convert.ToInt16(txtUserID.Text);
                    user.DeleteByID();
                    MessageBox.Show("User information deleted", "Bug Tracking Software");
                    loadGridContent();
                }
            }

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true)
            {
                MessageBox.Show("Please enter alphabets only", "Bug Tracker");
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtEmail.Text != "")
            {
                string strReg = "[\\w\\.-]*[a-zA-Z0-9_]@[\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]";
                Regex n = new Regex(strReg);
                Match v = n.Match(txtEmail.Text);
                if (!v.Success || txtEmail.Text.Length != v.Length)
                {
                    this.errorProvider1.SetError(txtEmail, "invalid e-mail format");
                }
                else
                {
                    this.errorProvider1.SetError(txtEmail, "");
                }
            }
        }

        private void txtContact_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtContact.ReadOnly == false)
            {
                if (e.KeyChar >= 65 && e.KeyChar <= 122 || e.KeyChar == 32)
                {
                    MessageBox.Show("Please enter numeric values only", "Bug Tracker");
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
        }
    }
}
