﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSoftware.AppCode;
namespace BugTrackingSoftware
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!CommonFunctions.UserType.Equals("Project Leader"))
            {
                reportToolStripMenuItem.Enabled = false;
            }

            if (CommonFunctions.UserType.Equals("Tester"))
            {
                userRegisterToolStripMenuItem.Enabled = false;
            }
            if (CommonFunctions.UserType.Equals("Developer"))
            {
                userRegisterToolStripMenuItem.Enabled = false;
            }

        }

        private void bugRegisterToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void projectRegistrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProjectRegistration obj = new frmProjectRegistration();
            obj.Show();
        }

        private void userRegisterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUserInfo obj = new frmUserInfo();
            obj.Show();
        }

        private void logOutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 login = new Form1();
            login.Show();
        }

        private void bugAssignToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CommonFunctions.UserType.Equals("Tester"))
            {
                frmBugList list = new frmBugList();
                list.Show();
            }

            if (CommonFunctions.UserType.Equals("Project Leader"))
            {
                frmBugList list = new frmBugList();
                list.Show();
            }
            if (CommonFunctions.UserType.Equals("Developer"))
            {
                frmDeveloperBugList list = new frmDeveloperBugList();
                list.Show();
            }
        }

        private void bugAssignToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //frmBugInfo info = new frmBugInfo(string userid, string bugid, string status);
            //info.Show();
        }

        private void bugListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReportBugList report = new frmReportBugList();
            report.Show();
        }

        private void bugRegistrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBugRegistration obj = new frmBugRegistration();
            obj.Show();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help obj = new Help();
            obj.Show();
        }
    }
}
