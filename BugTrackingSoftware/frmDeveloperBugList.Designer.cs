﻿namespace BugTrackingSoftware
{
    partial class frmDeveloperBugList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDeveloperList = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPriority = new System.Windows.Forms.Button();
            this.cmbPriority = new System.Windows.Forms.ComboBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBugDesc = new System.Windows.Forms.TextBox();
            this.txtBugTitle = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.currentDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBugID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeveloperList)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDeveloperList
            // 
            this.dgvDeveloperList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeveloperList.Location = new System.Drawing.Point(82, 546);
            this.dgvDeveloperList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvDeveloperList.Name = "dgvDeveloperList";
            this.dgvDeveloperList.ReadOnly = true;
            this.dgvDeveloperList.Size = new System.Drawing.Size(856, 252);
            this.dgvDeveloperList.TabIndex = 0;
            this.dgvDeveloperList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeveloperList_CellClick);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Fuchsia;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Indigo;
            this.label7.Location = new System.Drawing.Point(-2, -5);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(987, 83);
            this.label7.TabIndex = 87;
            this.label7.Text = "Bug Assign For Developer";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::BugTrackingSoftware.Properties.Resources._1454154384_Apply;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(759, 474);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 63);
            this.button1.TabIndex = 88;
            this.button1.Text = "Resolve Bug";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(818, 83);
            this.txtUserID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.ReadOnly = true;
            this.txtUserID.Size = new System.Drawing.Size(142, 26);
            this.txtUserID.TabIndex = 89;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnPriority);
            this.panel1.Controls.Add(this.cmbPriority);
            this.panel1.Controls.Add(this.cmbStatus);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtBugDesc);
            this.panel1.Controls.Add(this.txtBugTitle);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(82, 106);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(496, 430);
            this.panel1.TabIndex = 90;
            // 
            // btnPriority
            // 
            this.btnPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPriority.Image = global::BugTrackingSoftware.Properties.Resources._1454155162_Sync;
            this.btnPriority.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPriority.Location = new System.Drawing.Point(280, 366);
            this.btnPriority.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPriority.Name = "btnPriority";
            this.btnPriority.Size = new System.Drawing.Size(194, 57);
            this.btnPriority.TabIndex = 80;
            this.btnPriority.Text = "Change Status";
            this.btnPriority.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPriority.UseVisualStyleBackColor = true;
            this.btnPriority.Click += new System.EventHandler(this.btnPriority_Click);
            // 
            // cmbPriority
            // 
            this.cmbPriority.Enabled = false;
            this.cmbPriority.FormattingEnabled = true;
            this.cmbPriority.Items.AddRange(new object[] {
            "--Select Priority--",
            "High",
            "Severe",
            "Moderate",
            "Low"});
            this.cmbPriority.Location = new System.Drawing.Point(198, 228);
            this.cmbPriority.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbPriority.Name = "cmbPriority";
            this.cmbPriority.Size = new System.Drawing.Size(274, 33);
            this.cmbPriority.TabIndex = 79;
            // 
            // cmbStatus
            // 
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "In Progress"});
            this.cmbStatus.Location = new System.Drawing.Point(198, 292);
            this.cmbStatus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(274, 33);
            this.cmbStatus.TabIndex = 78;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 228);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 35);
            this.label2.TabIndex = 66;
            this.label2.Text = "Bug Priority";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 292);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(201, 35);
            this.label3.TabIndex = 65;
            this.label3.Text = "Bug Status";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 102);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 35);
            this.label4.TabIndex = 56;
            this.label4.Text = "Bug Description";
            // 
            // txtBugDesc
            // 
            this.txtBugDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBugDesc.Location = new System.Drawing.Point(198, 100);
            this.txtBugDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBugDesc.Multiline = true;
            this.txtBugDesc.Name = "txtBugDesc";
            this.txtBugDesc.ReadOnly = true;
            this.txtBugDesc.Size = new System.Drawing.Size(275, 96);
            this.txtBugDesc.TabIndex = 62;
            // 
            // txtBugTitle
            // 
            this.txtBugTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBugTitle.Location = new System.Drawing.Point(198, 43);
            this.txtBugTitle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBugTitle.Name = "txtBugTitle";
            this.txtBugTitle.ReadOnly = true;
            this.txtBugTitle.Size = new System.Drawing.Size(275, 30);
            this.txtBugTitle.TabIndex = 63;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 43);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 35);
            this.label9.TabIndex = 58;
            this.label9.Text = "Bug Title";
            // 
            // endDate
            // 
            this.endDate.CalendarForeColor = System.Drawing.Color.Red;
            this.endDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.endDate.CalendarTitleForeColor = System.Drawing.Color.DarkRed;
            this.endDate.Enabled = false;
            this.endDate.Location = new System.Drawing.Point(614, 222);
            this.endDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(326, 26);
            this.endDate.TabIndex = 94;
            this.endDate.Value = new System.DateTime(2016, 1, 13, 0, 0, 0, 0);
            this.endDate.ValueChanged += new System.EventHandler(this.endDate_ValueChanged);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(609, 168);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 35);
            this.label6.TabIndex = 93;
            this.label6.Text = "End Date";
            // 
            // currentDate
            // 
            this.currentDate.CalendarTitleBackColor = System.Drawing.Color.Chartreuse;
            this.currentDate.Location = new System.Drawing.Point(614, 375);
            this.currentDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.currentDate.Name = "currentDate";
            this.currentDate.Size = new System.Drawing.Size(326, 26);
            this.currentDate.TabIndex = 92;
            this.currentDate.Value = new System.DateTime(2016, 1, 13, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(609, 335);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 35);
            this.label5.TabIndex = 91;
            this.label5.Text = "Current Date";
            // 
            // txtBugID
            // 
            this.txtBugID.Location = new System.Drawing.Point(662, 83);
            this.txtBugID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBugID.Name = "txtBugID";
            this.txtBugID.ReadOnly = true;
            this.txtBugID.Size = new System.Drawing.Size(112, 26);
            this.txtBugID.TabIndex = 95;
            // 
            // frmDeveloperBugList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(980, 825);
            this.Controls.Add(this.txtBugID);
            this.Controls.Add(this.endDate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.currentDate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtUserID);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvDeveloperList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "frmDeveloperBugList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Developer Bug List";
            this.Load += new System.EventHandler(this.frmDeveloperBugList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeveloperList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDeveloperList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbPriority;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBugTitle;
        private System.Windows.Forms.TextBox txtBugDesc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker currentDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPriority;
        private System.Windows.Forms.TextBox txtBugID;
    }
}